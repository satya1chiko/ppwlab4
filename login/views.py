from django.shortcuts import render


# Create your views here.
def login(request):
    response = {}
    return render(request, 'login.html', response)
