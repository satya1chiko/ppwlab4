from django.urls import path
from .views import *

urlpatterns = [
    path('', intro, name='intro'),
    path('myhouse/', house, name='house'),
    path('myprofile/', index, name='index'),
    path('contacts/', contacts, name='contacts'),
    path('schedule/', schedule, name='schedule'),
    path('delete/', deleteall, name='delete')
]
