# Generated by Django 2.1.1 on 2018-10-04 04:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hari', models.CharField(max_length=15)),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField()),
                ('namaKeg', models.CharField(max_length=30)),
                ('tempat', models.CharField(max_length=40)),
                ('kategori', models.CharField(max_length=20)),
            ],
        ),
    ]
