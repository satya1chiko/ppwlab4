from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Kegiatan_Form
from .models import Kegiatan

# Create your views here.
mhs_name = 'Gusti Ngurah Agung Satya Dharma'
desc = '''Selamat datang di profile saya. Nama saya adalah Gusti
            Ngurah Agung Satya Dharma, dengan nama panggilan
            Satya. Saya tinggal di Jakarta dan sekarang sedang menjalani
            kuliah di Fasilkom UI dalam jurusan Sistem Informasi.
            Saya menganggap diri saya sebagai seseorang yang mampu
            bekerja keras dan rela untuk bekerjasama dengan semua jenis
            orang. Keahlian saya berada dalam programming.'''
pendidikan1 = 'SD Kembang         2005 - 2011'
pendidikan2 = 'SMP Pangudi Luhur    2011- 2014'
pendidikan3 = 'SMA Gonzaga          2014 - 2017'
pendidikan4 = 'Universitas Indonesia    2017 - Current'

address = ''' Jl. Warung Jati Timur 2 No. 17, RT 06 RW 04,
Kelurahan Kalibata, Kecamatan  Pancoran, 
Jakarta Selatan'''

desc2 = '''	Rumah saya merupakan rumah bertingkat dua. Terdapat 4 kamar tidur dan 2 kamar mandi. Terdapat sebuah dapur di
sebelah salah satu kamar mandidan terdapat halaman rumah yang cukup besar untuk menyimpan 2 mobil. Di halaman
tersebut terdapat juga tempat cucian. Di tingkat 2 terdapat tempatuntuk sembahyang (berdoa), dalam bentuk ruang suci dan merajan.'''

comp = 'Ada pipa yang bocor, Kamar mandi agak primitif, Ada banyak hama (kecoak dll)'


def index(request):
    response = {'name': mhs_name, 'desc': desc, 'pendidikan1': pendidikan1, 'pendidikan2': pendidikan2,
                'pendidikan3': pendidikan3, 'pendidikan4': pendidikan4}
    return render(request, 'index.html', response)


def intro(request):
    response = {}
    return render(request, 'intro.html', response)


def house(request):
    response = {'address': address, 'desc2': desc2, 'comp': comp}
    return render(request, 'house.html', response)


def contacts(request):
    response = {}
    return render(request, 'contacts.html', response)

def deleteall(request):
    Kegiatan.objects.all().delete()
    return HttpResponseRedirect("/schedule")

def schedule(request):
    if (request.method == 'POST'):
        form = Kegiatan_Form(request.POST)
        if (form.is_valid()):
            hari = request.POST['hari']
            tanggal = request.POST['tanggal']
            jam = request.POST['jam']
            namaKeg = request.POST['namaKeg']
            tempat = request.POST['tempat']
            kategori = request.POST['kategori']
            kegiatan = Kegiatan(hari=hari, tanggal=tanggal, jam=jam, namaKeg=namaKeg, tempat=tempat, kategori=kategori)
            kegiatan.save()
        else:
            return HttpResponseRedirect('../')

    else:
        form = Kegiatan_Form()
    html = 'schedule.html'
    all_p = Kegiatan.objects.all()

    return render(request, html, {'form': form, 'objects': all_p})
