from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    hari = models.CharField(max_length=15)
    tanggal = models.DateField()
    jam = models.TimeField()
    namaKeg = models.CharField(max_length=30)
    tempat=models.CharField(max_length=40)
    kategori = models.CharField(max_length=20)
    def __str__(self):
        return self.namaKeg
