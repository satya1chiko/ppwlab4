from django import forms

class Kegiatan_Form(forms.Form):
    hari = forms.CharField(max_length=15)
    tanggal = forms.DateField()
    jam = forms.TimeField()
    namaKeg = forms.CharField(max_length=30)
    tempat=forms.CharField(max_length=40)
    kategori = forms.CharField(max_length=20)